from rest_framework import serializers
from rest_framework.fields import CurrentUserDefault

from core.models import Message, User


class MessageSerializer(serializers.ModelSerializer):
    subject = serializers.CharField(required=False)

    class Meta:
        model = Message
        fields = ['pk', 'is_inbound', 'recipient', 'recipient_username',
                  'sender', 'sender_username', 'subject', 'text', 'created_at']
        read_only_fields = ['is_inbound', 'recipient', 'sender', 'sender_username', 'created_at']

    def create(self, validated_data):
        validated_data['is_inbound'] = False
        validated_data['sender'] = CurrentUserDefault()(self)
        # validated_data.pop('subject')
        return super().create(validated_data)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

    def create(self, validated_data):
        return User.objects.create_user(validated_data.pop('username'),
                                        validated_data.pop('password'),
                                        **validated_data)


# noinspection PyAbstractClass
class MessageBodyRequestSerializer(serializers.Serializer):
    pk = serializers.IntegerField()
