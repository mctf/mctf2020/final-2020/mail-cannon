from django.contrib.admin import ModelAdmin
from django.contrib.auth.admin import admin, UserAdmin
from django.contrib.auth.models import Group

from core import models

admin.site.unregister(Group)


@admin.register(models.User)
class UserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        ('Personal info', {'fields': ('first_name', 'last_name',)}),
        ('Permissions', {
            'fields': ('is_admin',),
        }),
        ('Important dates', {'fields': ('last_login',)}),
    )
    list_display = ('username', 'first_name', 'last_name', 'is_admin', 'created_at', 'modified_at',)
    list_filter = ('is_admin',)
    search_fields = ('username', 'first_name', 'last_name',)
    filter_horizontal = ()


@admin.register(models.Message)
class MessageAdmin(ModelAdmin):
    fieldsets = (
        ('Message from:', {'fields': ('sender', 'sender_username',)}),
        ('Message recipient:', {'fields': ('recipient', 'recipient_username',)}),
        ('Message content:', {'fields': ('text',)}),
    )
    list_display = ('sender', 'recipient', 'is_inbound', 'created_at')
    search_fields = ('sender_username', 'recipient_username', 'created_at')
    filter_horizontal = ()
