import logging

from django.contrib.auth.base_user import BaseUserManager

log = logging.getLogger('mail-cannon.core.managers')


class UserManager(BaseUserManager):
    def __create_user(self, username, password, **kwargs):
        log.debug('Creating user %s', username)
        if not username:
            raise ValueError('Username must be set.')
        user = self.model(username=username, **kwargs)
        user.set_password(password)
        user.save(using=self._db)
        log.info('Created user %s', repr(user))
        return user

    def create_user(self, username, password, **kwargs):
        kwargs.setdefault('is_admin', False)
        return self.__create_user(username, password, **kwargs)

    def create_superuser(self, username, password, **kwargs):
        kwargs.setdefault('is_admin', True)
        return self.__create_user(username, password, **kwargs)
