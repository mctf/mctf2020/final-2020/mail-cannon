import logging

from django.contrib.auth.base_user import AbstractBaseUser
from django.db import models

from core import managers
from core.validators import validate_email_user
from mail_cannon import settings

log = logging.getLogger('mail-cannon.core.models')


class TimeStampMixin(models.Model):
    created_at = models.DateTimeField('Created', auto_now_add=True)
    modified_at = models.DateTimeField('Modified', auto_now=True)

    class Meta:
        abstract = True


class User(AbstractBaseUser, TimeStampMixin):
    username = models.CharField(
        'Username',
        max_length=128,
        unique=True,
        validators=[validate_email_user, ],
        error_messages={
            'unique': 'A user with that username already exists.',
        },
    )
    first_name = models.CharField('First name', max_length=16)
    last_name = models.CharField('Last name', max_length=16)

    is_admin = models.BooleanField(
        'Staff status',
        default=False,
        help_text='Designates whether the user can log into this admin site.',
    )

    USERNAME_FIELD = 'username'
    EMAIL_FIELD = None
    REQUIRED_FIELDS = ['first_name', 'last_name', ]

    objects = managers.UserManager()

    def get_full_name(self):
        return self.__str()

    def get_short_name(self):
        return self.__str()

    def get_email(self):
        return f'{self.username}@[{settings.DEFAULT_IP}]'

    # noinspection PyUnusedLocal
    def has_perm(self, perm, obj=None):
        """DO NOT USE IN CLIENT CODE!"""
        return self.is_admin

    # noinspection PyUnusedLocal
    def has_module_perms(self, app_label):
        """DO NOT USE IN CLIENT CODE!"""
        return self.is_admin

    @property
    def is_staff(self):
        return self.is_admin

    @property
    def is_superuser(self):
        return self.is_admin

    def __str(self) -> str:
        return f'{self.first_name} {self.last_name} <{self.username}@[{settings.DEFAULT_IP}]>'.strip()

    def __str__(self):
        return self.__str()

    def __repr__(self):
        return f'<User {self.username=} {self.first_name=} {self.last_name=}, {self.is_admin=}>'

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'


class Message(TimeStampMixin):
    """
    # Message model;
    """
    is_inbound = models.BooleanField(default=False)
    recipient = models.ForeignKey('User', related_name='messages_sent',
                                  on_delete=models.CASCADE,
                                  null=True, blank=True)
    recipient_username = models.CharField(max_length=64)
    sender = models.ForeignKey('User', related_name='messages_received',
                               on_delete=models.CASCADE,
                               null=True, blank=True)
    sender_username = models.CharField(max_length=64)

    text = models.TextField()

    def __repr__(self):
        return f'Message<{self.recipient=} {self.sender=}>'

    def __str__(self):
        return f'Message from "{self.sender}" to "{self.recipient}"'
