import re

from django.core.exceptions import ValidationError
from django.utils.deconstruct import deconstructible


@deconstructible
class EmailUserValidator:
    message = 'Enter a valid username.'
    code = 'invalid'
    user_regex = re.compile(
        r"(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*\Z"
        r'|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-\011\013\014\016-\177])*"\Z)',
        re.IGNORECASE)

    def __init__(self, message=None, code=None):
        if message is not None:
            self.message = message
        if code is not None:
            self.code = code

    def __call__(self, value):
        if not value:
            raise ValidationError(self.message, code=self.code)

        if not self.user_regex.match(value):
            raise ValidationError(self.message, code=self.code)

    def __eq__(self, other):
        return (
                isinstance(other, EmailUserValidator) and
                (self.message == other.message) and
                (self.code == other.code)
        )


validate_email_user = EmailUserValidator()
