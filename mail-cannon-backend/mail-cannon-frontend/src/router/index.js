import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from '../views/Home.vue';
import Login from '../views/Login.vue';
import Register from '../views/Register.vue';
import Mailbox from '../views/Mailbox.vue';

Vue.use(VueRouter);

const routes = [
	{
		path: '/',
		name: 'Home',
		component: Home,
	},
	{
		path: '/login',
		name: 'Login',
		component: Login,
		meta: {title: 'Login',},
	},
	{
		path: '/register',
		name: 'Register',
		component: Register,
		meta: {title: 'Register',},
	},
	{
		path: '/mailbox/:type?',
		name: 'Mailbox',
		component: Mailbox,
		meta: {title: 'Mailbox',},
	},
];

const router = new VueRouter({
	routes,
	linkExactActiveClass: 'active',
});

router.afterEach((to) => {
	if (to?.meta?.title) {
		document.title = `${to.meta.title} — mail-cannon`;
	} else {
		document.title = 'mail-cannon';
	}
});

export default router;
