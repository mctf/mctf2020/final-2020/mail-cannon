from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from smtplib import SMTP, SMTPConnectError, SMTPServerDisconnected
from typing import List

import requests

STATUS_UP = 'UP'
STATUS_DOWN = 'DOWN'
STATUS_CORRUPT = 'CORRUPT'
STATUS_MUMBLE = 'MUMBLE'


class ApiException(Exception):
    status: str
    trace: str
    verbose_description: str

    def __init__(self, status, trace, verbose_description):
        self.status = status
        self.trace = trace
        self.verbose_description = verbose_description


def index(session: requests.Session, url: str):
    try:
        response = session.get(f'{url}/')
        response.raise_for_status()
    except requests.Timeout as e:
        raise ApiException(STATUS_DOWN, 'Connection timed out', str(e))
    except requests.ConnectionError as e:
        raise ApiException(STATUS_DOWN, 'Connection failed', str(e))
    except requests.HTTPError as e:
        raise ApiException(STATUS_MUMBLE, f'Unexpected status code {response.status_code}', str(e))
    except Exception as e:
        raise ApiException(STATUS_DOWN, 'Failed to get main page', str(e))


def register(session: requests.Session, url: str,
             username: str, password: str, first_name: str, last_name: str):
    try:
        response = session.post(f'{url}/api/register/', json={
            'first_name': first_name,
            'last_name': last_name,
            'username': username,
            'password': password,
        })
        response.raise_for_status()
    except requests.Timeout as e:
        raise ApiException(STATUS_DOWN, 'Connection timed out', str(e))
    except requests.ConnectionError as e:
        raise ApiException(STATUS_DOWN, 'Connection failed', str(e))
    except requests.HTTPError as e:
        raise ApiException(STATUS_MUMBLE, f'Failed to register: unexpected status code {response.status_code}', str(e))
    except Exception as e:
        raise ApiException(STATUS_DOWN, 'Failed to register', str(e))


def login(session: requests.Session, url: str,
          username: str, password: str) -> str:
    try:
        response = session.post(f'{url}/api/login/', json={
            'username': username,
            'password': password,
        })
        response.raise_for_status()
        return response.json()['token']
    except requests.Timeout as e:
        raise ApiException(STATUS_DOWN, 'Connection timed out', str(e))
    except requests.ConnectionError as e:
        raise ApiException(STATUS_DOWN, 'Connection failed', str(e))
    except requests.HTTPError as e:
        raise ApiException(STATUS_MUMBLE, f'Failed to log in: unexpected status code {response.status_code}', str(e))
    except KeyError as e:
        raise ApiException(STATUS_MUMBLE, 'Failed to log in: authorization token not found', str(e))
    except Exception as e:
        raise ApiException(STATUS_DOWN, 'Failed to log in', str(e))


def get_me(session: requests.Session, url: str) -> str:
    try:
        response = session.get(f'{url}/api/me/')
        response.raise_for_status()
        return response.json()['user_repr']
    except requests.Timeout as e:
        raise ApiException(STATUS_DOWN, 'Connection timed out', str(e))
    except requests.ConnectionError as e:
        raise ApiException(STATUS_DOWN, 'Connection failed', str(e))
    except requests.HTTPError as e:
        raise ApiException(STATUS_MUMBLE,
                           f'Failed to get user info: unexpected status code {response.status_code}', str(e))
    except KeyError as e:
        raise ApiException(STATUS_MUMBLE, 'Failed to get user info: user representation string not found', str(e))
    except Exception as e:
        raise ApiException(STATUS_DOWN, 'Failed to get user info', str(e))


def get_messages(session: requests.Session, url: str,
                 folder: str) -> List:
    try:
        response = session.get(f'{url}/api/messages/?folder={folder}')
        response.raise_for_status()
        return response.json()
    except requests.Timeout as e:
        raise ApiException(STATUS_DOWN, 'Connection timed out', str(e))
    except requests.ConnectionError as e:
        raise ApiException(STATUS_DOWN, 'Connection failed', str(e))
    except requests.HTTPError as e:
        raise ApiException(STATUS_MUMBLE,
                           f'Failed to get messages: unexpected status code {response.status_code}', str(e))
    except Exception as e:
        raise ApiException(STATUS_DOWN, 'Failed to get messages', str(e))


def get_message_body(session: requests.Session, url: str,
                     message_id: int) -> str:
    try:
        response = session.post(f'{url}/api/message/', json={
            'pk': message_id,
        })
        response.raise_for_status()
        return response.json()['data']
    except requests.Timeout as e:
        raise ApiException(STATUS_DOWN, 'Connection timed out', str(e))
    except requests.ConnectionError as e:
        raise ApiException(STATUS_DOWN, 'Connection failed', str(e))
    except requests.HTTPError as e:
        raise ApiException(STATUS_MUMBLE,
                           f'Failed to get message body: unexpected status code {response.status_code}', str(e))
    except KeyError as e:
        raise ApiException(STATUS_MUMBLE, 'Failed to get message body: \'data\' key not found', str(e))
    except Exception as e:
        raise ApiException(STATUS_DOWN, 'Failed to get message body', str(e))


def send_email(ip: str, username: str, subject: str, body: str, timeout: int):
    try:
        msg = MIMEMultipart()
        msg['From'] = f'{username}@[{ip}]'
        msg['To'] = f'{username}@[{ip}]'
        msg['Subject'] = subject
        msg.attach(MIMEText(body, 'plain'))
        with SMTP(ip, timeout=timeout) as smtp:
            smtp.noop()
            smtp.rset()
            smtp.sendmail(msg['From'], msg['To'], msg.as_string())
    except SMTPConnectError as e:
        raise ApiException(STATUS_DOWN, 'Failed to connect to SMTP server', str(e))
    except SMTPServerDisconnected as e:
        raise ApiException(STATUS_MUMBLE, 'SMTP server disconnected', str(e))
    except Exception as e:
        raise ApiException(STATUS_DOWN, 'Failed to send email', str(e))


def send_email_server_api(session: requests.Session, url: str, to: str, subject: str, body: str) -> str:
    try:
        response = session.post(f'{url}/api/messages/', json={
            'recipient_username': to,
            'subject': subject,
            'text': body,
        })
        response.raise_for_status()
        return response.json()['pk']
    except requests.Timeout as e:
        raise ApiException(STATUS_DOWN, 'Connection timed out', str(e))
    except requests.ConnectionError as e:
        raise ApiException(STATUS_DOWN, 'Connection failed', str(e))
    except requests.HTTPError as e:
        raise ApiException(STATUS_MUMBLE,
                           f'Failed to send email: unexpected status code {response.status_code}', str(e))
    except KeyError as e:
        raise ApiException(STATUS_MUMBLE, 'Failed to parse \'send email\' response: \'pk\' key not found', str(e))
    except Exception as e:
        raise ApiException(STATUS_DOWN, 'Failed to send email', str(e))
