import sys

from mail_cannon import check_functionality

redisHost = sys.argv[1]
redisPassword = sys.argv[2]
ip = sys.argv[3]

status, trace, stderr = check_functionality(ip)
print(status)
print(trace, flush=True)
print(stderr, file=sys.stderr, flush=True)
