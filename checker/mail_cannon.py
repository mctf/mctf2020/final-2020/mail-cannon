import random
import string

from requests.adapters import HTTPAdapter, Response
from user_agent import generate_user_agent

from api import *

timeout = 3
fake_exploits = [
    ' SELECT 1 ',
    ' UNION 1,2 ',
    ' ../../../../etc/passwd ',
    '; && cat /etc/passwd ',
    '; /bin/bash -c "echo test" #',
    '''<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE foo [
<!ELEMENT foo ANY >
<!ENTITY xxe SYSTEM "file:///etc/passwd" >]>
<foo>&xxe;</foo>'''
]

folders = [
    'inbound',
    'outgoing',
    'all',
]


class TimeoutHTTPAdapter(HTTPAdapter):
    def send(self, *args, **kwargs) -> Response:
        kwargs['timeout'] = timeout
        return super().send(*args, **kwargs)


def generate_str(length=10):
    """Generate a random string of fixed length."""
    letters = string.ascii_letters + string.digits
    return ''.join(random.choice(letters) for _ in range(length))


def generate_extended_str(length=10):
    """Generate a random string of fixed length."""
    letters = string.ascii_letters + string.digits + r'!#$%&()*+,-./:;<=>?@[]^_`{|}~'
    return ''.join(random.choice(letters) for _ in range(length))


def get_fake_exploit():
    return random.choice(fake_exploits)


def check_functionality(ip):
    url = f'http://{ip}:65501'
    try:
        with requests.Session() as session:
            session.mount('https://', TimeoutHTTPAdapter(max_retries=2))
            session.mount('http://', TimeoutHTTPAdapter(max_retries=2))
            session.verify = False
            session.headers['User-Agent'] = generate_user_agent()

            username = generate_str()
            password = generate_str()
            first_name = generate_str()
            last_name = generate_str()

            index(session, url)
            register(session, url, username, password, first_name, last_name)
            token = login(session, url, username, password)
            session.headers['Authorization'] = f'Token {token}'
            server_username = get_me(session, url)

            if username not in server_username:
                raise ApiException(STATUS_MUMBLE, 'Invalid username from server',
                                   f'Registered username={username}, server username={server_username}')

            subject = generate_str()
            body = generate_extended_str(30) + get_fake_exploit()
            send_email_server_api(session, url, f'{username}@[{ip}]', subject, body)

            messages = get_messages(session, url, random.choice(folders))
            if len(messages) == 0:
                raise ApiException(STATUS_MUMBLE, 'Sent message not found',
                                   'len(messages) == 0')
            get_message_body(session, url, random.choice(messages)['pk'])

    except ApiException as e:
        return e.status, e.trace, e.verbose_description
    return STATUS_UP, 'All functionality checks passed', ''


def push_flag(ip, flag):
    url = f'http://{ip}:65501'
    try:
        with requests.Session() as session:
            session.mount('https://', TimeoutHTTPAdapter(max_retries=2))
            session.mount('http://', TimeoutHTTPAdapter(max_retries=2))
            session.verify = False
            session.headers['User-Agent'] = generate_user_agent()

            username = generate_str()
            password = generate_str()
            first_name = generate_str()
            last_name = generate_str()
            subject = generate_str()
            body = '<code>' + get_fake_exploit() + ' ' + flag + ' ' + generate_str(30) + '</code>'

            register(session, url, username, password, first_name, last_name)
            token = login(session, url, username, password)
            session.headers['Authorization'] = f'Token {token}'

            send_email(ip, username, subject, body, timeout)

    except ApiException as e:
        return '', '', e.status, e.trace, e.verbose_description
    return username, password, STATUS_UP, 'Flag successfully pushed', ''


def pull_flag(ip, username, password, flag):
    url = f'http://{ip}:65501'
    try:
        with requests.Session() as session:
            session.mount('https://', TimeoutHTTPAdapter(max_retries=2))
            session.mount('http://', TimeoutHTTPAdapter(max_retries=2))
            session.verify = False
            session.headers['User-Agent'] = generate_user_agent()

            token = login(session, url, username, password)
            session.headers['Authorization'] = f'Token {token}'

            messages = get_messages(session, url, 'inbound')
            found_pk = None
            for message in messages:
                if flag in message['text']:
                    found_pk = message['pk']
                    break

            if found_pk is None:
                raise ApiException(STATUS_CORRUPT, 'Flag not found', 'Not found in email text (non-parsed)')

            message_body = get_message_body(session, url, found_pk)

            if flag not in message_body:
                raise ApiException(STATUS_CORRUPT, 'Flag not found', 'Not found in email body (parsed)')

    except ApiException as e:
        return e.status, e.trace, e.verbose_description
    return STATUS_UP, 'Flag successfully pulled', ''


def local_test():
    ip = '192.168.1.2'
    flag = generate_str(32)

    status, trace, stderr = check_functionality(ip)
    print(f'CHECK status={status}, trace={trace}, stderr={stderr}')
    username, password, status, trace, stderr = push_flag(ip, flag)
    print(f'PUSH  status={status}, trace={trace}, stderr={stderr}')
    status, trace, stderr = pull_flag(ip, username, password, flag)
    print(f'PULL  status={status}, trace={trace}, stderr={stderr}')


if __name__ == '__main__':
    local_test()
