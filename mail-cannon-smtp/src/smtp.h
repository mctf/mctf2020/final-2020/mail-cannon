// Half-rewritten, half-inspired-by https://gist.github.com/PhirePhly/2914635
#ifndef MAIL_CANNON_SMTP_SMTP_H
#define MAIL_CANNON_SMTP_SMTP_H

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <libpq-fe.h>

#include "globals.h"
#include "log.h"

#define STREQ(a, b) (strcmp(a, b) == 0)

#define BUFF_SIZE 8192
#define MAX_EMAIL_LEN 254


static const char REQ_HELLO[] = "HELO";
static const char REQ_MAIL[] = "MAIL";
static const char REQ_RECIPIENT[] = "RCPT";
static const char REQ_DATA[] = "DATA";
static const char REQ_RESET[] = "RSET";
static const char REQ_NOOP[] = "NOOP";
static const char REQ_QUIT[] = "QUIT";
static const char REQ_DEBG[] = "DEBG";

static const char RESP_SERVER_GREETING[] = "220 %s mail-cannon is ready~\r\n";
static const char RESP_COMMAND_TOO_LONG[] = "500 It's so fucking big.\r\n";
static const char RESP_OKAY[] = "250 Yep.\r\n";
static const char RESP_OKAY_DEBG[] = "250 Here it is: %s\r\n";
static const char RESP_BYE[] = "221 %s Sayonara~\r\n";
static const char RESP_COMMAND_NOT_IMPLEMENTED[] = "502 WTF?!\r\n";
static const char RESP_CONTINUE[] = "354 GIVE ME YOUR DATA.\r\n";
static const char RESP_USER_NOT_FOUND[] = "550 No such user here.\r\n";
static const char RESP_INVALID_ARGUMENTS[] = "501 Huh?!\r\n";
static const char RESP_INVALID_DOMAIN[] = "551 Wrong domain; please try <%s@%s>.\r\n";
static const char RESP_SERVER_ERROR[] = "554 Internal Server Error.\r\n";
static const char RESP_NOT_ENOUGH_DATA[] = "450 Not enough data to send email.\r\n";

void process_conversation(PGconn* db_connection, int socket_fd, char* client_ip, char* server_ip);

#endif //MAIL_CANNON_SMTP_SMTP_H
