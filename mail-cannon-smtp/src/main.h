#ifndef MAIL_CANNON_SMTP_MAIN_H
#define MAIL_CANNON_SMTP_MAIN_H

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <getopt.h>
#include <libpq-fe.h>
#include <pthread.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "log.h"
#include "globals.h"
#include "utils.h"
#include "smtp.h"

static const char short_options[] = "vd:t:";

static const struct option long_options[] = {
		{"verbose", no_argument,       0, 'v'},
		{"db",      required_argument, 0, 'd'},
		{"threads", required_argument, 0, 't'},
		{0, 0,                         0, 0}
};

typedef struct {
	int socket_fd;
	char* db_connection_string;
} process_tcp_thread_arg_t;

static pthread_mutex_t log_mutex;


int main(int argc, char* argv[]);

void* process_tcp_thread(void* thread_arg);

static void lock_callback(bool lock, void* udata);

#endif //MAIL_CANNON_SMTP_MAIN_H
