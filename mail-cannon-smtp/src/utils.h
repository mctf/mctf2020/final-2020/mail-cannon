#ifndef MAIL_CANNON_SMTP_UTILS_H
#define MAIL_CANNON_SMTP_UTILS_H

#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/socket.h>
#include <netinet/in.h>

typedef enum {
	STR2INT_SUCCESS,
	STR2INT_OVERFLOW,
	STR2INT_UNDERFLOW,
	STR2INT_INCONVERTIBLE
} str2int_errno;

str2int_errno str2long(long* out, char* s, int base);

void* get_in_addr(struct sockaddr* sa);

#endif //MAIL_CANNON_SMTP_UTILS_H
